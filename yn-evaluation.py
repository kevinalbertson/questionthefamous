import extraction
import qa

question_data = {
    "Franklin_Roosevelt" : [
        ("Are you president", "yes"),
        ("Are you not president", "no"),
        ("Have you ever been governor of New York", "yes"),
        ("Is it true that you have never been governor of New York", "no"),
        ("Are you a politian", "yes"),
        ("Are you American", "yes"),
        ("Are you Russian", "no"),
        ("Are you deceased", "yes"),
        ("Are you living", "no"),
        ("Are you married", "yes")
    ],
    "Bill_Nye" : [

    ],
    "Abraham_Lincoln" : [

    ],
    "Bill_Gates" : [
        ("Did you ever commit a crime", "yes"),
        ("Did you never commit a crime", "no")
    ],
    "Malcom_Gladwell" : [

    ]
}


def evaluate():
    correct_sum = 0
    num_questions = 0
    for name in question_data.keys():
        print "%s%s%s" % ("=" * 10, name , "=" * 10)
        segments = extraction.getSentences(name)
        num_questions += len(question_data[name])
        for (q,a) in question_data[name]:
            print ">> %s" % q
            name_parts = name.split("_")
            (qtype, responses) = qa.getAnswer(segments, q, name_parts[0], name_parts[1])
            print responses
            if a in responses:
                correct_sum += 1
                print "Correct"
            else:
                print "Incorrect"

    print "%s Report %s" % ("=" * 10 , "=" * 10)
    print "There were %s questions" % (num_questions)
    print "The proportion correct is (%s / %s) = %s" % (correct_sum, num_questions, correct_sum * 1.0 / num_questions)

evaluate()
