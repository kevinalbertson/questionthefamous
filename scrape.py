import urllib2

people = [
  {
    "name" : "Franklin_Roosevelt",
    "wiki" : "https://en.wikipedia.org/wiki/Franklin_D._Roosevelt"
  },
  {
    "name" : "Bill_Gates",
    "wiki" : "http://en.wikipedia.org/wiki/Bill_Gates"
  },
  {
    "name" : "Abraham_Lincoln",
    "wiki" : "https://en.wikipedia.org/wiki/Abraham_Lincoln"
  },
  {
    "name" : "Bill_Nye",
    "wiki" : "https://en.wikipedia.org/wiki/Bill_Nye"
  },
  {
    "name" : "Malcom_Gladwell",
    "wiki" : "http://en.wikipedia.org/wiki/Malcolm_Gladwell"
  }
]
def scrapeSources():
    '''
    Crawls through online articles to find people's biographys
    '''
    for p in people:
        print "Fetching %s " % p["name"]
        response = urllib2.urlopen(p["wiki"])
        html = response.read()
        out = open("data/%s.html" % p["name"], "w")
        out.write(html)
        out.close()

scrapeSources()
