import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from operator import itemgetter
from nltk.tree import Tree
from nltk.stem.snowball import SnowballStemmer

def rank(items, fn, keep_scores=False):
    '''
    Given a list of items, rank them based on the function fn
    If fn returns 0, it ignores the item
    '''
    ranked = [(fn(i), i) for i in items if fn(i) > 0]
    ranked = sorted(ranked, key=itemgetter(0), reverse=True)
    if keep_scores:
        return ranked
    else:
        return [i for (rank, i) in ranked]

def keywordRank(keywords):
    '''
    Ranks based on keyword density
    '''
    def rank(s):
        return sum([1 for w in s if w in keywords]) * 1.0 / len(w)
    return rank

def keywordStemRank(keywords):
    '''
    Ranks based on keyword density
    '''
    stemmer = SnowballStemmer("english")
    stemmed_keywords = [stemmer.stem(w) for w in keywords]
    def rank(s):
        stemmed_words = [stemmer.stem(w) for w in s]
        return sum([1 for w in stemmed_words if w in stemmed_keywords]) * 1.0 / len(w)
    return rank


def neRank():
    def rank(s):
        tagged = nltk.pos_tag(s)
        cnt = 0
        chunked = nltk.ne_chunk(tagged)
        #count the number of named entities
        for item in chunked:
            if type(item) == Tree:
                cnt += 1
        return cnt * 1.0 / len(s)
    return rank

def databaseKeywordRank(keywords, pos):
    '''
    Ranks based on keyword count
    @param {int} pos is an integer in [0,2] representing which column to rank from
    '''
    def rank(entry):
        return sum([1 for w in entry[pos] if w in keywords])
    return rank
