var qtypes = require("qtypes");
var question = process.argv[2];
new qtypes(function(classifier){
  var question_type = classifier.questionType(question);
  var question_class = classifier.classify(question);
  console.log(question_type + "|" + question_class);
})
