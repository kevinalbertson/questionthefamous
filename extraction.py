from bs4 import BeautifulSoup
import nltk

def getText(soup):
    text = soup.get_text()
    text = text.encode("ascii", errors="replace")
    text = text.replace("?", " ") #replace unknowns with " "
    return text

def getParagraphs(filename="Franklin_Roosevelt"):
    file = open('data/%s.html' % filename, 'r')
    raw = file.read()
    file.close()
    soup = BeautifulSoup(raw)
    ps = soup.find_all("p")
    paragraphs = []
    for p in ps:
        text = getText(p)
        words = nltk.tokenize.word_tokenize(text)
        words = [w for w in words]
        paragraphs.append(words)
    return paragraphs

def getInfoCard(filename="Franklin_Roosevelt"):
    '''
    Returns the ninja info card
    Data entries are tuples:
    (key, value, topic)
    Topic is the heading given in the card, which is optional.

    @param {String} filename
    '''
    file = open('data/%s.html' % filename, 'r')
    raw = file.read()
    file.close()
    soup = BeautifulSoup(raw)
    infocard = soup.find(class_="infobox")
    rows = infocard.find_all("tr")
    topic_text = []
    data = []
    for r in rows:
        header = r.find("th")
        value = r.find("td")
        if header and value:
            header_text = nltk.word_tokenize(getText(header).lower())
            value_text = nltk.word_tokenize(getText(value).replace("\n", ". "))
            data_entry = (header_text, value_text, topic_text)
            data.append(data_entry)
        elif header:
            #this is a header-only row, make this the current topic
            topic_text = nltk.word_tokenize(getText(header).lower())
    return data

def getSentences(filename="Franklin_Roosevelt"):
    paragraphs = getParagraphs(filename)
    sentences = []
    for p in paragraphs:
        s = []
        for w in p:
            if w == ".":
                sentences.append(s)
                s = []
            else:
                s.append(w)
    return sentences

def getParagraphsOnlyWithLinks(filename="Franklin_Roosevelt"):
    file = open('data/%s.html' % filename, 'r')
    raw = file.read()
    file.close()
    soup = BeautifulSoup(raw)
    ps = soup.find_all("p")
    paragraphs = []
    for p in ps:
        link_tags = p.find_all("a")
        if len(link_tags) == 0:
            continue
        text = getText(p)
        words = nltk.tokenize.word_tokenize(text)
        words = [w for w in words]
        paragraphs.append(words)
    return (paragraphs)