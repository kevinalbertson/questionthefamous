import os
import nltk
from nltk.parse import stanford

os.environ['STANFORD_PARSER'] = 'stanford-parser'
os.environ['STANFORD_MODELS'] = 'stanford-parser'
#java_path = "C:/Program Files/Java/jre1.8.0_40/bin/java.exe"
#os.environ['JAVAHOME'] = java_path

parser = stanford.StanfordParser(model_path="stanford-parser/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")
sent = nltk.word_tokenize("The light blue and red box.")
sent = nltk.word_tokenize("Franklin Delano Roosevelt (January 30, 1882 April 12, 1945), commonly known by his initials FDR, was an American statesman and political leader who served as the President of the United States.")
tagged = nltk.pos_tag(sent)
parsed = parser.tagged_parse(tagged)

#print parsed


def _findShallowestVerbPhrase(root):
	'''
	BFS to go through tree and return
	shallowest VP node
	'''
	q = [root]
	while len(q) > 0:
		node = q.pop(0)
		if node.label() == "VP":
			return node
		for child in node:
			if type(child) == nltk.tree.Tree:
				q.append(child)		
	return None

