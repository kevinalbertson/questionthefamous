import extraction
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.tree import Tree
import sys
import subprocess
from rank import *
import parsing


def make_keywords(sentence):
    '''
    Generates keywords with wordnet, removes stopwords
    @param {list} sentence
    @return {list}
    '''
    keywords = []
    #remove stop words
    filtered = [w for w in sentence if w not in stopwords.words("english")]
    #add similar words from wordnet
    for w in filtered:
        for synset in wordnet.synsets(w):
            for syn in synset.lemma_names():
                '''
                As of now, there isn't support for phrase searching, so
                synonym phrases are ignored
                '''
                if syn.find("_") == -1:
                    keywords.append(syn)
    return keywords



def reflectAnswer(answer, firstname, lastname):
    '''
    Makes a 3rd person statement into a 1st person one
    @param {list} answer
    @param {string} firstname
    @param {string} lastname
    '''
    tagged = nltk.pos_tag(answer)
    reflected = []
    reflectWords = False
    for (w,t) in tagged:
        word = w
        if w == firstname or w == lastname:
            word = "I"
            #reflect the next 3rd person verb (tagged as VBZ)
            reflectWords = True
            if w == lastname:
                continue
        elif t == "VBZ" and reflectWords:
            if(w == "is"):
                word = "am"
            elif(word[-1:] == "s"):
                word = word[0:-1]
        elif t == "PRP$" and reflectWords:
            word = "my"
        elif t[0:1] == "N":
            reflectWords = False
        reflected.append(word)
    return reflected

def extractAnswer(question, answer_words):
    if answer_words[0] == "yes" or answer_words[0] == "no":
        return answer_words

    (question_type, question_class) = classifyQuestion(question)

    primary = None
    ne_possibilities = []
    tag_possibilities = []
    if question_class is not None:
        primary = question_class.split(":")[0]

    if primary == "ENTY":
        ne_possibilities = ["GPE"]
    elif primary == "HUM":
        ne_possibilities = ["PERSON"]
    elif primary == "LOC":
        ne_possibilities = ["GPE"]
    elif primary == "NUM":
        tag_possibilities = ["CD"]

    tagged = nltk.pos_tag(answer_words)
    chunked = nltk.ne_chunk(tagged)

    for p in chunked:
        if type(p) is nltk.tree.Tree:
            #label is the type
            if p.label() in ne_possibilities:
                return [w for (w,pos) in p.leaves()]
        elif p[1] in tag_possibilities:
            return [p[0]]
    return ["I", "cannot", "say"]

def getFactoidAnswer(segments, question, firstname, lastname, question_type, question_class):
    question_words = nltk.tokenize.word_tokenize(question)
    question_words = [w.lower() for w in question_words]

    keywords = make_keywords(question_words)
    #keywords.append(firstname)
    #keywords.append(lastname)

    #initially rank paragraphs based on keyword count
    ranked_segments = rank(segments, keywordRank(keywords))
    if len(ranked_segments) == 0:
        return []

    #take the top 10, do additional heavy processing
    top10 = [s for s in ranked_segments][:10]
    ranked_top10 = rank(top10, neRank())
    reflected = [reflectAnswer(s, firstname, lastname) for s in ranked_top10]
    return reflected

def getYesNoAnswer(segments, question, firstname, lastname, question_type, question_class):
    question_words = nltk.word_tokenize(question)
    filtered = [w for w in question_words if w not in stopwords.words("english")]
    count = 0
    for s in segments:
        for w in filtered:
            if w in s:
                count += 1
    if count > 0:
        return [["yes"]]
    else:
        return [["no"]]


def getDatabaseAnswer(database, question, firstname, lastname, question_type, question_class):
    # check for matches in the header with question words
    question_words = nltk.word_tokenize(question)
    question_words = [w for w in question_words if w not in stopwords.words("english")]
    ranked = []
    ranked_with_score = rank(database, databaseKeywordRank(question_words, 0), keep_scores=True)
    if len(ranked_with_score) > 1:
        highest_score = ranked_with_score[0][0]
        highest_entries = [r[1] for r in ranked_with_score if r[0] == highest_score]
        if len(highest_entries) > 1:
            #break ties with topic
            ranked = rank(highest_entries, databaseKeywordRank(question_words, 2))
            if len(ranked) == 0:
                #nothing found, revert to first ranking
                ranked = [r[1] for r in ranked_with_score]
    else:
        ranked = [r[1] for r in ranked_with_score] #strip score
    #return only the values
    return [r[1] for r in ranked]

def classifyQuestion(question):
    out = subprocess.check_output(["node", "question_classify.js", question])
    parts = out.split("|")
    if parts[0] == "":
        parts[0] = None
    if parts[1] == "\n":
        parts[1] = None
    else:
        parts[1] = parts[1][:-1]
    return (parts[0], parts[1])

def getAnswer(segments, database, question, firstname, lastname):
    #because the current question classifier has trouble with "were" questions, I'm replacing them with "are"
    question = question.replace("Were", "Are")
    question = question.replace("were", "are")
    if (question == "Who are you"):
        desc = parsing.getDescription(segments[0], firstname, lastname)
        return ("DESC", [reflectAnswer(desc, firstname, lastname)])

    (question_type, question_class) = classifyQuestion(question)
    if question_type == "YN":
        # yes no question
        return ("YN", getYesNoAnswer(segments, question, firstname, lastname, question_type, question_class))
    else:
        # first try database
        db_answers = getDatabaseAnswer(database, question, firstname, lastname, question_type, question_class)
        if len(db_answers) > 0:
            return ("FACTOID", db_answers)
        else:
            return ("FACTOID", getFactoidAnswer(segments, question, firstname, lastname, question_type, question_class))


def printResponse(question, qtype, response):
    if qtype == "YN":
        print response[0][0]
    elif qtype == "FACTOID":
        if len(response) == 0:
            print "I cannot say"
        else:
            print " ".join(extractAnswer(question, response[0]))
    elif qtype == "DESC":
        print " ".join(response[0])

def start():
    name_string = "Franklin_Roosevelt"
    (firstname, lastname) = name_string.split("_")
    segments = extraction.getSentences(name_string)
    database = extraction.getInfoCard(name_string)

    questions = [
        "Who are you",
        "Were you a president"
        "What is the name of your dog"
    ]

    for q in questions:
        print "Asking >> %s " % q
        (qtype, response) = getAnswer(segments, database, q, firstname, lastname)
        printResponse(q, qtype, response)

def prompt():
    name_string = "Franklin_Roosevelt"
    (firstname, lastname) = name_string.split("_")
    segments = extraction.getSentences(name_string)
    database = extraction.getInfoCard(name_string)
    instructions = [
        "You are now chatting with a famous person",
        "He/she will only respond to questions",
        "Exit the program by entering 'exit'"
    ]
    maxlen = max([len(s) for s in instructions])
    print "%s" % ("=" * maxlen)
    for s in instructions:
        print s
    print "%s" % ("=" * maxlen)
    while True:
        q = raw_input(">> ")
        if q == "exit" or q == "quit":
            print "Bye!"
            break
        if q[-1] == "?":
            q = q[:-1]
        (qtype, response) = getAnswer(segments, database, q, firstname, lastname)
        printResponse(q, qtype, response)

prompt()
