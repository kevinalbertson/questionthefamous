import extraction
import qa

question_data = {
    "Franklin_Roosevelt" : [
        ("When were you born", "1882"),
        ("What year were you born", "1882"),
        ("Where were you born", "new york"),
        ("What was your nationality", "dutch"),
        ("When did you die", "1945"),
        ("Where did you die", "georgia"),
        ("What year did you die", "1945"),
        ("Where did you go to school", "harvard"),
        ("How did you die", "stroke"),
        ("What was your occupation", "president"),
        ("What illness did you have", "polio"),
        ("What was the name of your wife", "eleanor"),
        ("Which president were you", "32nd"),
        ("How many terms did you serve", "three"),
        ("What did you institute", "new deal"),
        ("Which president did you defeat", "hoover"),
        ("What war did you participate in", "world world"),
        ("What was the name of your dog", "fala"),
        ("During which term did you die", "fourth"),
        ("What rank are you in terms of presidents", "three"),
        ("What was the name of your fourth male child", "franklin"),
        ("Where were you paralyzed", "waist"),
        ("When did you start in office", "1933"),
        ("Who preceded you as governor of new york", "smith"),
        ("Who succeeded you as governor of new york", "woodbury"),
        ("Who was the governor after you in new york", "woodbury")
    ],
    "Bill_Nye" : [
        ("When were you born", "1955"),
        ("What year were you born", "1955"),
        ("Where were you born", "washington"),
        ("Where did you go to school", "cornell"),
        ("What was your occupation", "scientist"),
        ("Where do you live", "los angeles"),
        ("What do you wear", "lab coat")
    ],
    "Abraham_Lincoln" : [
        ("When were you born", "1809"),
        ("How did you die", "assasination"),
        ("What war did you live during", "civil"),
        ("Who assasinated you", "booth"),
        ("Who was the commanding general", "lee"),
        ("Where were you born", "kentucky"),
        ("What was the name of your mother", "nancy"),
        ("How old were you when you died", "56")
    ],
    "Bill_Gates" : [
        ("When were you born", "1955"),
        ("What are you founder of", "microsoft"),
        ("Who is the new CEO of Microsoft", "nadella"),
        ("Where were you born", "washington"),
        ("What is the name of your sister", "kristi"),
        ("What algorithm did you devise", "pancake"),
        ("What school did you attend", "harvard"),
        ("What operating system did you make", "windows"),
        ("What case were you involved in", "united states v. microsoft"),
        ("What religion are you", "catholic")
    ],
    "Malcom_Gladwell" : [
        ("What is your third book", "outliers"),
        ("What is your first book", "tipping point"),
        ("What is your occupation", "writer"),
        ("What is your job", "writer"),
        ("Where do you work", "new yorker"),
        ("What was your major in college", "history")
    ]
}


def evaluate():
    rank_sum = 0
    num_questions = 0
    for name in question_data.keys():
        print "%s%s%s" % ("=" * 10, name , "=" * 10)
        segments = extraction.getSentences(name)
        database = extraction.getInfoCard(name)
        num_questions += len(question_data[name])
        for (q,a) in question_data[name]:
            print ">> %s" % q
            name_parts = name.split("_")
            (qtype, responses) = qa.getAnswer(segments, database, q, name_parts[0], name_parts[1])
            rank = 1
            for r in responses:
                r = " ".join(r).lower()
                if a in r:
                    print "\tFound (%s) %s" % (rank, r)
                    break
                else:
                    print "\tNot Found (%s) %s" % (rank, r)
                rank += 1
            if len(responses) == 0:
                rank = 10
                print "No response, (%s)" % (rank)
            rank_sum += (1.0/rank)
    print "%s Report %s" % ("=" * 10 , "=" * 10)
    print "There were %s questions" % (num_questions)
    print "The Mean Reciprocal Rank is %s" % ((1.0/num_questions) * rank_sum)

evaluate()
