'''
When I try using MaceCommand from NLTK, although it runs without throwing errors
, it always returns false.
'''

import nltk
from nltk import load_parser
parser = load_parser('./roosevelt-grammar.fcfg', trace=0)

def parse(sentence):
    sentence = sentence.lower()
    tokens = sentence.split()
    expr = ""
    for tree in parser.parse(tokens):
        expr = "%s%s" % (expr, tree.label()['SEM'])
    return expr

sentences = [
    "Roosevelt attended Harvard",
    "All classmates attended Harvard"
]

for s in sentences:
    print "%s%s%s" % ("=" * 10, "Original Sentence", "=" * 10)
    print s
    print "%s%s%s" % ("=" * 10, "First Order Logic", "=" * 10)
    print parse(s)
    print ""
