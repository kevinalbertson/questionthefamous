import os
import nltk
from nltk.parse import stanford

os.environ['STANFORD_PARSER'] = 'stanford-parser'
os.environ['STANFORD_MODELS'] = 'stanford-parser'
java_path = "/usr/bin/java"#C:/Program Files/Java/jre1.8.0_40/bin/java.exe"
os.environ['JAVAHOME'] = java_path
parser = stanford.StanfordParser(model_path="stanford-parser/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")

def getDescription(words, firstname, lastname):
	tagged = nltk.pos_tag(words)
	parsed = parser.tagged_parse(tagged)
	vp = _findShallowestVerbPhrase(parsed)
	ans = [firstname, lastname]
	ans.extend(vp.leaves())
	return ans

def _findShallowestVerbPhrase(root):
	'''
	BFS to go through tree and return
	shallowest VP node
	'''
	q = [root]
	while len(q) > 0:
		node = q.pop(0)
		if node.label() == "VP":
			return node
		for child in node:
			if type(child) == nltk.tree.Tree:
				q.append(child)		
	return None

