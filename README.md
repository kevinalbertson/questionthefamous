Relevant articles are included in the 'papers' directory.

The main file is qa.py. To run this, you need the following installed:

- NLTK
- NLTK datasets (obtained from nltk.download())
- Stanford parser
- NodeJS
- qtypes package for NodeJS

Then you can run it simply with `python qa.py`

The proof-of-concept for the feature-based grammar is in `semantics.py`

The main evaluation (factoid) is in `factoid-evaluation.py`. There is also a simple yes/no evaluation in `yn-evaluation.py`